import java.io.*;
class p2{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter The No Of Rows : ");

		int row=Integer.parseInt(br.readLine());
		int num=row;
		for(int i=1;i<=row;i++){
			int num2=row-i;
			for(int j=1;j<=row;j++){

				if(num2>0){
					System.out.print((char)(96+num)+" ");
				}
				else{
					System.out.print((char)(64+num)+" ");
				}
				num2--;
				num++;
			}
			System.out.println();
		}
	}
}
