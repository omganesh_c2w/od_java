import java.io.*;
class pattern1{
    public static void main(String args[])throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter The Row=");
        int row=Integer.parseInt(br.readLine());
        for(int i=1;i<row;i++){
            for(int j=i;j<=row;j++){
                System.out.print(j+" ");
            }
            System.out.println();
        }
    }
}