
import java.io.*;
class p5{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter The Size : ");

		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		int temp=0;
		for(int i=0;i<size/2;i++){
			temp=arr[i];
			arr[i]=arr[size-i-1];
			arr[size-i-1]=temp;
		}
		for(int i=0;i<arr.length;i++){
			System.out.println("Reversed array : "+arr[i]);
		}
	}
}
