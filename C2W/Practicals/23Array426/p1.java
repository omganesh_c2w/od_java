import java.io.*;
class p1{
	public static void main(String []args)throws IOException{
			BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Enter The Size : ");

			int size=Integer.parseInt(br.readLine());

			int arr[]=new int[size];
			int sum=0;
			for(int i=0;i<arr.length;i++){
				arr[i]=Integer.parseInt(br.readLine());
				sum=sum+arr[i];
			}
			int avg=sum/arr.length;
			System.out.println("Avg Of An Array : "+avg);
		}
	}
