import java.io.*;
class p6{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
		System.out.print("Enter The Size : ");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=br.readLine().charAt(0);
		}
		for(int j=0;j<arr.length;j++){
			if(arr[j]!='a' && arr[j]!='e' && arr[j]!='i' && arr[j]!='o' && arr[j]!='u'){
				System.out.print((char)(arr[j])+" ");
			}
		}

	}
}
