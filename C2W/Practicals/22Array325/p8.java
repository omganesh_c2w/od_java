import java.io.*;
class p8{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter The Size : ");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int j=0;j<arr.length;j++){
			int cnt=0;
			int num=arr[j];
			while(num>0){
				if(arr[j]%num==0){
					cnt++;
				}
				num--;
			}
			if(cnt>2){
				System.out.println("Composite No. in Array :"  + arr[j]);
			}
		}
	}
}

