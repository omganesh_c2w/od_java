import java.io.*;
class p10{
        public static void main(String []args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter The No Of Rows : ");

                int row=Integer.parseInt(br.readLine());

                for(int i=1;i<=row;i++){
                        int num=row-i+1;
                        for(int sp=1;sp<i;sp++){
                                System.out.print("\t");
                        }
                        for(int j=1;j<=(row-i)*2+1;j++){
                                if(j<row-i+1){
                                        System.out.print(num+"\t");
                                        num--;
                                }
                                else{
                                        System.out.print(num+"\t");
                                        num++;
                                }
                        }
                        System.out.println();
                }
        }
}

