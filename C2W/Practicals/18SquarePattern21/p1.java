import java.io.*;
class p1{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter The No Of Rows : ");

		int row=Integer.parseInt(br.readLine());
		
		for(int i=1;i<=row;i++){
			int num=row;	
			for(int j=1;j<=row;j++){
				if(i%2==1){
					System.out.print((char)(64+num)+" ");
					num--;
				}
				else{
					System.out.print(num+" ");
				}
			}
			System.out.println();
		}
	}
}

