import java.io.*;
class p8{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter The Size : ");
		int size=Integer.parseInt(br.readLine());
		char arr[]=new char[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=br.readLine().charAt(0);
		}
		for(int i=0;i<arr.length/2;i++){
			char temp=arr[i];
			arr[i]=arr[size-1-i];
			arr[size-1-i]=temp;
		}
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
	}
}
