import java.io.*;
class p4{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());
		System.out.print("Enter The No Of Rows : ");
		for(int i=1;i<=row;i++){
			int  num=i;
			for(int sp=1;sp<=i-1;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=row-i+1;j++){
				if(row%2==1){
					
					System.out.print((char)(64+num)+"\t");
				}
				else{
					System.out.print((char)(96+num)+"\t");
				}
				num++;
			}
			System.out.println();
		}
	}
}


